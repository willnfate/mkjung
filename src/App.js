import React, { Component } from 'react'
import { getLocal } from './config'

const axios = require('axios')
axios.defaults.headers.common['Pragma'] = 'no-cache'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.baseURL = 'https://s3.ap-northeast-2.amazonaws.com/bucketplace-coding-test/cards'

import Card from './Card'
import './App.css'
import btCheck from './img/bt-checkbox-checked.svg'

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      page: 0,
      photos: [],
      isLoad: false,
      isLast: false,
      isOnlyScrap: false
    }
  }

  componentDidMount() {
    this.handleList()
    window.addEventListener('scroll', this.handleScroll, true)
  }

  /**
   * 사진 리스트 호출
   */
  handleList = () => {
    // * 이미 리스트를 모두 불러온 후 다시 스크롤 했을 경우 추가 호출 방지 설정
    if (this.state.isLast) return null

    axios.get(`/page_${this.state.page + 1}.json`)
      .then(
        res => {
          const { photos } = this.state
          this.setState({
            page: this.state.page + 1,
            isLoad: true,
            isLast: res.data.length < 20,
            photos: photos.concat(res.data)
          })
        },
        error => {
          console.error(error)
        }
      )
  }

  /**
   * 스크롤 이벤트 처리
   */
  handleScroll = e => {
    if ((window.scrollY + window.innerHeight) >= document.body.scrollHeight) {
      // * 추가 리스트 호출
      this.handleList()
    }
  }

  /**
   * 스크랩 필터
   */
  handleFilter = () => {
    const { isOnlyScrap } = this.state
    this.setState({ isOnlyScrap: !isOnlyScrap })
  }

  /**
   * 사진 렌더 처리
   */
  handleRender = () => {
    const { isOnlyScrap, photos } = this.state
    const list = isOnlyScrap ? getLocal('scraps') : photos

    let cols = [], rows = []
    rows = list.map((o, i) => {
      if (i % 4 === 0) cols = []
      cols.push(<Card key={`card-${o.id}`} item={o} />)
      if ((i + 1) % 4 === 0 || i === list.length - 1) {
        return <div key={`row-${i + 1 / 4}`} className="CARD-Container">{cols}</div>
      }
    })
    return rows
  }

  render() {

    // * 사진 목록이 없는 경우 렌더 방지
    if (!this.state.isLoad) return <div />

    return (
      <div className="PC-__">
        <div className="PC-Head" onClick={this.handleFilter}>
          <img src={btCheck} className="bt_checkbox_checked" alt="check" />
          <div className="PC-Head-Content">
            스크랩한 것만 보기
          </div>
        </div>
        <div className="PC-Body">
          {this.handleRender()}
        </div>
      </div >
    )
  }
}

export default App