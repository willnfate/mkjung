// * 배열의 특정 값을 가진 객체 구하기
Array.prototype.find = function (value) {
  let idx = this.findIndex(x => x.id === value)
  return !!this[idx]
}
// * 배열에서 특정 값을 가진 객체 삭제
Array.prototype.remove = function (value) {
  let idx = this.findIndex(x => x.id === value)
  this.splice(idx, 1)
  return this
}

// * 로컬 스토리지 관리
const localStorage = window.localStorage
export const setLocal = (key, value) => {
  if (value) localStorage.setItem(key, JSON.stringify(value))
}
export const getLocal = (key) => {
  const data = localStorage.getItem(key)
  // console.log(data)
  return data ? JSON.parse(localStorage.getItem(key)) : []
}