import React, { Component } from 'react'
import { setLocal, getLocal } from './config'

import avatar from './img/ic-avatar-cat.svg'
import scrapOff from './img/on-img.svg'
import scrapOn from './img/blue.svg'

// * 스크랩 버튼을 누른 경우 localStorage를 이용하여 스크랩한 사진 정보를 저장
let scraps = getLocal('scraps')

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: scraps.length > 0 ? scraps.find(this.props.item.id) : false
    }
  }

  // * 스크랩 추가/제거
  handleScrap = () => {
    const { isToggleOn } = this.state
    const { item } = this.props
    if (!confirm(isToggleOn ? '스크랩 해제하시겠습니까?' : '스크랩 하시겠습니까?')) return true
    if (isToggleOn) {
      setLocal('scraps', scraps.remove(item.id))
    } else {
      setLocal('scraps', scraps.concat(item))
    }
    scraps = getLocal('scraps')
    this.setState({ isToggleOn: !isToggleOn })
  }

  render() {
    const { item } = this.props
    return (
      <div className="CARD">
        <div className="CARD-Head">
          <img src={avatar} className="ic_avatar_cat" alt="avatar" />
          <div className="CARD-Head-Content">
            {item.nickname}
          </div>
        </div>
        <div className="CARD-Content">
          <img src={item.image_url} className="Rectangle" alt="card" />
          <img src={this.state.isToggleOn ? scrapOn : scrapOff} className="on-img" alt="scrap"
            onClick={this.handleScrap} />
        </div>
      </div>
    )

  }
}

export default Card